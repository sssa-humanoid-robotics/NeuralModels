# Neural Models

![License](https://img.shields.io/badge/license-GPLv2-green.svg)

This repository hosts spike-based computational models of sensory afferents and actuators, as well as networks that make use of them. The models and networks are implemented for the NEST and/or SpiNNaker simulation platforms.

Currently, the repository includes the following models:

* Ia and II afferents from muscle spindles [sensory, NEST, SpiNNaker]
* muscle fiber twitches integration [actuation, NEST]
* regular and irregular vestibular afferents [sensory, NEST]

And the following network circuits:

* spinal cord network [NEST]
* vestibulo-ocular reflex network [NEST]


## Installing and running (NEST)
The models are provided within two extension modules for [NEST](http://www.nest-simulator.org/) >= 2.18.0, and are built with [cmake](https://cmake.org/). NEST enviroments variables must be included in the `PATH` as described in the NEST installation istructions.

```sh
$ cd nest
$ make
$ make install
```

Any module can also be built independently with:
```sh
$ cd nest
$ mkdir build-module && cd build-module
$ cmake ../module
$ make
$ make install
```

For more details on installing NEST extension modules, please refer to the [official documentation](https://nest.github.io/nest-simulator/extension_modules).

After a successful installation, the models should be available from both SLI and PyNest and the examples should run.

## Installing and running (SpiNNaker)
The model is provided as new neuron type for [SpiNNaker](https://spinnakermanchester.github.io/2016.001.AnotherFineProductFromTheNonsenseFactory/) 2016.001. Before using it, the SpiNNaker development environment must be set up as described [here](https://spinnakermanchester.github.io/2016.001.AnotherFineProductFromTheNonsenseFactory/spynnaker/PyNNOnSpinnakerDeveloperInstall.html).  Then it can be compiled with the following:

```sh
$ cd spinnaker/c_models
$ make
```

After a succesful compilation and after setting the PYTHONPATH environment variable to include the `spinnaker` folder, the example provided should run.


### License

This program is open source software and is licensed under the [GNU General Public License v2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html).


### Citing

Please cite the following articles if you use these models in your work:

Vannucci L., Falotico F., Laschi C., **"Proprioceptive Feedback through a Neuromorphic Muscle Spindle Model"**, 2017, *Frontiers in Neuroscience*, 11, 341 [(link)](http://journal.frontiersin.org/article/10.3389/fnins.2017.00341)

Here are suitable BibTeX entries:

```latex
@ARTICLE{vannucci:proprioceptive,
  author={Vannucci, Lorenzo and Falotico, Egidio and Laschi, Cecilia},
  title={Proprioceptive Feedback through a Neuromorphic Muscle Spindle Model},
  journal={Frontiers in Neuroscience},
  volume={11},      
  pages={341},
  year={2017},
  url={http://journal.frontiersin.org/article/10.3389/fnins.2017.00341},
  doi={10.3389/fnins.2017.00341},
  issn={1662-453X},
}
```
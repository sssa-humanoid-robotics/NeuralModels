/*
 *  twitches_integrator.cpp
 *
 *  Copyright (C) 2018 Lorenzo Vannucci
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "twitches_integrator.h"

// C++ includes:
#include <limits>
#include <cmath>

// Includes from libnestutil:
#include "numerics.h"

// Includes from nestkernel:
#include "exceptions.h"
#include "universal_data_logger_impl.h"

// Includes from sli:
#include "dict.h"
#include "dictutils.h"
#include "doubledatum.h"
#include "integerdatum.h"
#include "lockptrdatum.h"

#include "actuators_names.h"

using namespace nest;

namespace actuators {

/* ----------------------------------------------------------------
 * Recordables map
 * ---------------------------------------------------------------- */

nest::RecordablesMap<twitches_integrator> twitches_integrator::recordablesMap_;

}

namespace nest
{
// Override the create() method with one call to RecordablesMap::insert_()
// for each quantity to be recorded.
template <>
void
RecordablesMap<actuators::twitches_integrator>::create()
{
    // use standard names whereever you can for consistency!
    insert_(actuators::names::activation, &actuators::twitches_integrator::get_activation_);

    insert_("debug", &actuators::twitches_integrator::get_debug_);
}
}

namespace actuators {

/* ----------------------------------------------------------------
 * Parameters and state
 * ---------------------------------------------------------------- */

twitches_integrator::Parameters_::Parameters_() {

    p.clear();
    Ti.clear();

    num_of_receptors_ = 0;
    receptor_types_.clear();

}

twitches_integrator::State_::State_(const twitches_integrator::Parameters_& p) {

    activation = 0.0;
    acts.resize(p.num_of_receptors_);
    acts_1.resize(p.num_of_receptors_);
    acts_2.resize(p.num_of_receptors_);

}


/* ----------------------------------------------------------------
 * Parameter and state extractions and manipulation functions
 * ---------------------------------------------------------------- */

void twitches_integrator::Parameters_::get(DictionaryDatum& d) const {

    ArrayDatum p_(p);
    def<ArrayDatum>(d, names::p, p_);

    ArrayDatum Ti_(Ti);
    def<ArrayDatum>(d, names::Ti, Ti_);

}

void twitches_integrator::Parameters_::set(const DictionaryDatum& d) {

    std::vector<double> p_tmp;
    if (updateValue<std::vector<double>>(d, names::p, p_tmp)) {
        p = p_tmp;
        num_of_receptors_ = p.size();
    }

    std::vector<double> Ti_tmp;
    if (updateValue<std::vector<double>>(d, names::Ti, Ti_tmp)) {
        if (Ti_tmp.size() != num_of_receptors_)
            throw BadProperty("Vectors p and Ti must be of the same size.");
        Ti = Ti_tmp;
    }

    for (size_t i = 0; i < num_of_receptors_; i++) {
        if (p[i] <= 0 || Ti[i] <= 0)
            throw BadProperty("Elements of vectors p and Ti must be positive.");
    }

    receptor_types_.resize(num_of_receptors_);
    for (size_t i = 0; i < num_of_receptors_; i++) {
        receptor_types_[i] = i+1;
    }

}

void twitches_integrator::State_::get(DictionaryDatum& d) const {

    (*d)[names::activation] = activation;

}

void twitches_integrator::State_::set(const DictionaryDatum& ds, const twitches_integrator::Parameters_& p) {

    updateValue<double>(ds, names::activation, activation);

    acts.resize(p.num_of_receptors_);
    acts_1.resize(p.num_of_receptors_);
    acts_2.resize(p.num_of_receptors_);

    lastspike.resize(p.num_of_receptors_);

}

twitches_integrator::Buffers_::Buffers_(twitches_integrator& n)
    :logger_(n) {}

twitches_integrator::Buffers_::Buffers_(const twitches_integrator::Buffers_& b, twitches_integrator& n)
    :spikes_(b.spikes_), logger_(n) {}


/* ----------------------------------------------------------------
 * Default and copy constructor for muscle model
 * ---------------------------------------------------------------- */

twitches_integrator::twitches_integrator()
    :Archiving_Node(),
      P_(),
      S_(P_),
      B_(*this) {

    recordablesMap_.create();

    // cannot be changed after nodes are created
    V_.time_step = Time::get_resolution().get_ms()/1000.0;

    //    std::cout << "(twitches_integrator) timestep = " << V_.time_step << std::endl;

}

twitches_integrator::twitches_integrator(const twitches_integrator& n)
    :Archiving_Node(n),
      P_(n.P_),
      S_(n.S_),
      B_(n.B_, *this) {

    // cannot be changed after nodes are created
    V_.time_step = Time::get_resolution().get_ms()/1000.0;


}



/* ----------------------------------------------------------------
 * Muscle initialization functions
 * ---------------------------------------------------------------- */

void twitches_integrator::init_state_(const Node& proto) {

    const twitches_integrator& pr = downcast<twitches_integrator>(proto);
    S_ = pr.S_;

}

void twitches_integrator::init_buffers_() {

    B_.spikes_.resize(P_.num_of_receptors_);
    B_.logger_.reset();

}

void twitches_integrator::calibrate() {

    B_.logger_.init();

}


/* ----------------------------------------------------------------
 * Update and spike handling functions
 * ---------------------------------------------------------------- */

void twitches_integrator::update(const Time& slice_origin, const long from_step, const long to_step) {

    assert(to_step >= 0 && (delay)from_step < kernel().connection_manager.get_min_delay());
    assert(from_step < to_step);

    for ( long lag = from_step; lag < to_step; ++lag ) {

        S_.activation = 0.0;

        for (unsigned int i = 0; i < P_.num_of_receptors_; i++) {

            S_.acts[i] = 2*std::exp(-V_.time_step/P_.Ti[i])*S_.acts_1[i] - std::exp(-2*V_.time_step/P_.Ti[i])*S_.acts_2[i];

            double spike = B_.spikes_[i].get_value(lag);
            if (spike > 0) {


                // non linear scaling
                double spiketime = slice_origin.get_ms()/1000.0 + lag*V_.time_step;
                double isi = spiketime - S_.lastspike[i];
                double g = 1.0;
                double x = P_.Ti[i]/isi;
                if (!V_.firstspike && x > 0.4)
                    g = (1-std::exp(-2*std::pow(x, 3)))/(x)/V_.g_norm;

                S_.debug = g;

                S_.acts[i] += P_.p[i]*g*V_.time_step*V_.time_step/P_.Ti[i]*std::exp(1-V_.time_step/P_.Ti[i])*spike;

                S_.lastspike[i] = spiketime;
                V_.firstspike = false;

//                std::cout << "(twitches_integrator) spike detected at time " << spiketime << " , isi = " << isi << "\n";

            }

            S_.activation += S_.acts[i];

        }

        // normalization
        S_.activation /= V_.fullnorm;


        B_.logger_.record_data( slice_origin.get_steps() + lag );

        S_.acts_2 = S_.acts_1;
        S_.acts_1 = S_.acts;

    }



}

void twitches_integrator::calibrate_normalization() {

    V_.g_norm = (1.0-std::exp(-2*std::pow(0.4, 3)))/(0.4);

    double T = V_.time_step;
    V_.fullnorm = 0.0;
    for (unsigned int i = 0; i < P_.num_of_receptors_; i++) {

        double p = P_.p[i];
        double Ti = P_.Ti[i];

        double L = p/V_.g_norm * std::pow(T,3)/std::pow(Ti,2) * (1.0-std::exp(-1*std::pow(Ti/T, 3))) * std::exp(1.0-T/Ti) / (1.0 - 2*std::exp(-T/Ti) + std::exp(-2*T/Ti));
        V_.fullnorm += L;

    }

    // std::cout << "(twitches_integrator) fullnorm = " << V_.fullnorm << std::endl;

}


/* ----------------------------------------------------------------
 * Event handlers
 * ---------------------------------------------------------------- */

void twitches_integrator::handle(SpikeEvent& e) {

    assert(e.get_delay_steps() > 0);

    if (B_.spikes_.size() < P_.num_of_receptors_)
        B_.spikes_.resize(P_.num_of_receptors_);

    for (size_t i = 0; i < P_.num_of_receptors_; ++i) {

        if (P_.receptor_types_[i] == e.get_rport()) {
            B_.spikes_[i].add_value(e.get_rel_delivery_steps(kernel().simulation_manager.get_slice_origin()),
                                    e.get_weight() * e.get_multiplicity());
        }

    }

}

void twitches_integrator::handle(DataLoggingRequest& e) {
    B_.logger_.handle(e);
}

}

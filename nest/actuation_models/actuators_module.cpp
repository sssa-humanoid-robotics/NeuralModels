/*
 *  actuators_module.cpp
 *
 *  Copyright (C) 2018 Lorenzo Vannucci
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "actuators_module.h"

// Generated includes:
#include "config.h"

#include "twitches_integrator.h"

// Includes from nestkernel:
#include "dynamicloader.h"
#include "exceptions.h"
#include "genericmodel.h"
#include "genericmodel_impl.h"
#include "kernel_manager.h"
#include "model.h"
#include "model_manager_impl.h"
#include "nestmodule.h"
#include "target_identifier.h"

// Includes from sli:
#include "booldatum.h"
#include "integerdatum.h"
#include "sliexceptions.h"
#include "tokenarray.h"

// -- Interface to dynamic module loader ---------------------------------------
#if defined( LTX_MODULE ) | defined( LINKED_MODULE )
actuators::ActuatorsModule actuators_module_LTX_mod;
#endif

// -- DynModule functions ------------------------------------------------------

actuators::ActuatorsModule::ActuatorsModule() {
#ifdef LINKED_MODULE
    nest::DynamicLoaderModule::registerLinkedModule( this );
#endif
}

actuators::ActuatorsModule::~ActuatorsModule() {}

const std::string actuators::ActuatorsModule::name( void ) const {
    return std::string( "Actuators Module" ); // Return name of the module
}

const std::string actuators::ActuatorsModule::commandstring( void ) const {
    // Instruct the interpreter to load actuators_module-init.sli
    return std::string( "(actuators_module-init) run" );
}

//-------------------------------------------------------------------------------------

void actuators::ActuatorsModule::init(SLIInterpreter*) {
    nest::kernel().model_manager.register_node_model< twitches_integrator >("twitches_integrator");
}

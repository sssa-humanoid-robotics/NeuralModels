import nest
from nest import raster_plot

from nest_networks.spikes import get_spikes_from_detector
from nest_networks.recordingplayer import RecordingPlayer

# generate some spikes via Poisson gereators at different frequencies
pg = nest.Create('poisson_generator', 10)
for i in range(len(pg)):
    nest.SetStatus([pg[i]], {'rate': i*100.0})

sd = nest.Create('spike_detector', 1)
nest.Connect(pg, sd)

nest.Simulate(1000)

# collect spikes from detector
spikes = get_spikes_from_detector(sd)

# reset nest simulation
nest.ResetKernel()

# reproduce while multipling the sources by 10 and adding some gaussian noise
rp = RecordingPlayer(spikes, mult=10, sigma=5.)

sd = nest.Create('spike_detector', 1)
nest.Connect(rp.get_gids(), sd)

nest.Simulate(1000)

# plot results
raster_plot.from_device(sd)
raster_plot.show()

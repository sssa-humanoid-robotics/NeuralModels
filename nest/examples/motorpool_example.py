import nest
import pylab

from nest_networks.spinalcord import MotorPool

# motorpool parameters
params = {
    'dmn': 8.5,
    'dmx': 24.0,
    'DSF': 1e-6/120.0*109.37,
    'cspf': 1e-2,
    'taumax': 6.5e-3,
    'tauadj': 60e-6,
    'tauslp': 9.44e1,
    'pmx': 15.0,
    'pmn': 2.25,
    'FSF': 1.0/15.0*18.19,
    'smn': 0.104,
    'ssl': 0.207,
    'TSF': 0.6892,
    'Ncal': 740
}

# create motorpool
mp = MotorPool(100, params)

# simulate some descending alpha
descending_alpha = nest.Create('poisson_generator', 100, {'rate': 10.0, 'stop': 1000.0})
nest.Connect(descending_alpha, mp.motoneurons, 'all_to_all', {'weight': 5.0, 'delay': 1.0})

# record muscle activation
mul = nest.Create('multimeter', params={'record_from': ['activation']})
nest.Connect(mul, mp.muscle)

nest.Simulate(2000)

# plot muscle activation
events = nest.GetStatus(mul)[0]["events"]
t = events["times"]
pylab.plot(t, events["activation"])
pylab.xlabel("time (ms)")
pylab.ylabel("activation")
pylab.legend("bcl")
pylab.show()

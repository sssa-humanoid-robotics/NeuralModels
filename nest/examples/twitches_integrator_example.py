# -*- coding: utf-8 -*-
#
# twitches_integrator_example.py
#
# Copyright (C) 2018 Lorenzo Vannucci
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import nest
from nest import raster_plot
import pylab
import numpy

nest.Install("actuators_module")

# simulation parameters
sim_time = 1000
pop_size = 100

# RNG
msd = 123456
pyrngs = numpy.random.RandomState(msd)

# generation of twitches
motoneurons = nest.Create('poisson_generator', pop_size, [ {'rate': pyrngs.uniform(250, 500)} for i in range(pop_size) ] )

# twitches integration model
p = [pyrngs.uniform(20, 60) for x in range(pop_size)]
Ti = [pyrngs.uniform(0.01, 0.2) for x in range(pop_size)]
muscle = nest.Create('twitches_integrator', 1)
nest.SetStatus(muscle, {'p': p, 'Ti': Ti})

for i in range(pop_size):
    nest.Connect([motoneurons[i]], muscle, 'one_to_one', {'receptor_type': i + 1})

# activation recorder
mult = nest.Create('multimeter', params={'record_from': ['activation']})
nest.Connect(mult, muscle)

# simulation
nest.Simulate(sim_time)

# activation plot
events = nest.GetStatus(mult)[0]['events']
t = events['times'];
pylab.plot(t, events['activation'])
pylab.axis([0, sim_time, -0.1, 1.1])
pylab.ylabel('activation')
pylab.xlabel('time [ms]')
pylab.show()
from nest_networks.spinalcord import SpinalCord
from nest_networks.spinalcord.muscles_human import biceps, brachialis, triceps, flexors, extensors, flex_extend
import nest
import pylab
from nest import raster_plot

# spinal cord parameters
descflag = False
cord_flags = {

    'excitatory_II': True,
    'homonymus': True,
    'heteronymus': True,
    'polysynaptic': True,
    'descending': descflag,
    'scale': 0.1

}

# initialize spinal cord network
sc = SpinalCord(cord_flags,
                [biceps, brachialis, triceps],
                [flexors, extensors],
                [flex_extend])

io = sc.get_IO_populations()

# record spikes from all populations
recordable = sc.get_all_recordable_gids()
sd = nest.Create('spike_detector', 1)
nest.Connect(recordable, sd)

# record muscle activations
mlist = io['biceps_cb_m'] + io['biceps_cl_m'] + io['brachialis_m'] + io['triceps_cla_m'] + io['triceps_cm_m'] + io['triceps_clo_m']
mul = nest.Create('multimeter', params={'record_from': ['activation']})
nest.Connect(mul, mlist)

# integrated descending stimuli
gamma = 75.0
if descflag:
    nest.SetStatus(io['biceps_descending_alpha'], 'rate', 2000.0)
    nest.SetStatus(io['brachialis_descending_alpha'], 'rate', 1500.0)
    nest.SetStatus(io['triceps_descending_alpha'], 'rate', 2000.0)
    nest.SetStatus(io['biceps_descending_gamma_dyn'], 'rate', gamma)
    nest.SetStatus(io['brachialis_descending_gamma_dyn'], 'rate', gamma)

else:

    # external stimuli
    mydesc = []
    for i in range(9):
        if i % 3 == 0:
            mydesc.append(nest.Create('poisson_generator', 1, {'rate': 2000.0})[0])
        else:
            mydesc.append(nest.Create('poisson_generator', 1, {'rate': 0.0})[0])

    desc = {
        'biceps': {'alpha': nest.Create('poisson_generator', 1, {'rate': 2000.0}),
                   'gamma_st': nest.Create('poisson_generator', 1, {'rate': gamma}),
                   'gamma_dyn': nest.Create('poisson_generator', 1, {'rate': gamma})},
        'brachialis': {'alpha': nest.Create('poisson_generator', 1, {'rate': 1500.0})},
        'triceps': {'alpha': nest.Create('poisson_generator', 1, {'rate': 2000.0})}
    }
    sc.attach_descending(desc)

# simulation, reach resting state
nest.set_verbosity('M_WARNING')
nest.Simulate(500)


# stretch stimulation
duration = 200
speed = 1.5
L = 1.0
for i in range(duration // 2):  # lenghtening
    nest.SetStatus(io['biceps_cb_Ia'], {'L': L, 'dL': speed})
    nest.SetStatus(io['biceps_cb_II'], {'L': L, 'dL': speed})
    nest.SetStatus(io['biceps_cl_Ia'], {'L': L, 'dL': speed})
    nest.SetStatus(io['biceps_cl_II'], {'L': L, 'dL': speed})
    nest.SetStatus(io['brachialis_Ia'], {'L': L, 'dL': speed})
    nest.SetStatus(io['brachialis_II'], {'L': L, 'dL': speed})
    L = L + speed/1000.
    nest.Simulate(1)
for i in range(duration // 2):  # shortening
    nest.SetStatus(io['biceps_cb_Ia'], {'L': L, 'dL': -speed})
    nest.SetStatus(io['biceps_cb_II'], {'L': L, 'dL': -speed})
    nest.SetStatus(io['biceps_cl_Ia'], {'L': L, 'dL': -speed})
    nest.SetStatus(io['biceps_cl_II'], {'L': L, 'dL': -speed})
    nest.SetStatus(io['brachialis_Ia'], {'L': L, 'dL': -speed})
    nest.SetStatus(io['brachialis_II'], {'L': L, 'dL': -speed})
    L = L - speed/1000.
    nest.Simulate(1)

# simulation, after stimulation
nest.Simulate(500-duration)


# plots
tit = ""
fil = ""
if duration > 0:
    tit = tit + " (reflex"
else:
    tit = tit + " (no reflex"

if gamma > 0.0:
    tit = tit + (", gamma = %.1f)" % gamma)
else:
    tit = tit + ", no gamma)"

pylab.figure()
events = nest.GetStatus(mul)[0]["events"]
legends=['biceps_cb', 'biceps_cl', 'brachialis', 'triceps_cla', 'triceps_cm', 'triceps_clo']
for i in range(len(mlist)):
    m = mlist[i]
    t = [x for (x, y) in zip(events["times"], events["senders"]) if y == m]
    a = [x for (x, y) in zip(events["activation"], events["senders"]) if y == m]
    pylab.plot(t, a, label=legends[i])
pylab.xlabel("time (ms)")
pylab.ylabel("activation level")
pylab.legend()
pylab.title("muscles activations" + tit)
pylab.show()

raster_plot.from_device(sd)
pylab.title("neural activity" + tit)
raster_plot.show()
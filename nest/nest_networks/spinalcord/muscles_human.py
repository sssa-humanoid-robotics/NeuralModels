# Warning: parameters in this file are not properly tuned

# spindle parameters
spindle_params_Ia = {
    'bag1_G': 20000.0 / 5.0,
    'bag2_G': 10000.0 / 5.0,
    'chain_G': 10000.0 / 5.0
}

spindle_params_II = {
    'bag1_G': 20000.0 / 5.0,
    'bag2_G': 10000.0 / 5.0,
    'chain_G': 10000.0 / 5.0,
    'primary': False
}

# muscles parameters
biceps_params = {
    'dmn': 8.5,
    'dmx': 60,  # 120.0,
    'DSF': 1e-6/120.0*109.37,
    'cspf': 1e-2,
    'taumax': 2.0e-3,  # 12.5e-3,
    'tauadj': 60e-6,
    'tauslp': 9.44e1,
    'pmx': 15.0,
    'pmn': 2.25,
    'FSF': 1.0/15.0*18.19,
    'smn': 0.104,
    'ssl': 0.207,
    'TSF': 0.6892,
    'Ncal': 740
}

# spinal cord settings (muscles)
biceps = {
    'name': 'biceps',
    'mn_params': biceps_params,
    'Ia_params': spindle_params_Ia,
    'II_params': spindle_params_II,
    'bundles': ['cl', 'cb'],
    'motoneurons': 774,
    'spindles': 320
}

brachialis = {
    'name': 'brachialis',
    'mn_params': biceps_params,  # this is wrong
    'Ia_params': spindle_params_Ia,
    'II_params': spindle_params_II,
    'motoneurons': 681,
    'spindles': 256
}

triceps = {
    'name': 'triceps',
    'mn_params': biceps_params,  # this is wrong
    'Ia_params': spindle_params_Ia,
    'II_params': spindle_params_II,
    'bundles': ['clo', 'cm', 'cla'],
    'motoneurons': 1571,
    'spindles': 520
}

# spinal cord settings (groups)
flexors = {'name': 'flexors',
           'muscles': ['biceps', 'brachialis'],
           'n_II_interneurons': int(196.0/169 * (biceps['motoneurons']+brachialis['motoneurons']))
}

extensors = {'name': 'extensors',
             'muscles': ['triceps'],
             'n_II_interneurons': int(196.0/169 * triceps['motoneurons'])
}

# spinal cord settings (antagonistic)
flex_extend = {'g1': {'group': 'flexors', 'n_Ia_interneurons': int(196.0/169 * triceps['motoneurons'])},
               'g2': {'group': 'extensors', 'n_Ia_interneurons': int(196.0/169 * (biceps['motoneurons']+brachialis['motoneurons']))}}

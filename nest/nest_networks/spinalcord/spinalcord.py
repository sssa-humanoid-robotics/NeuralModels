import nest

from .motorpool import MotorPool

# NEST modules can only be loaded once
try:
    nest.Install("sensors_module")
except nest.NESTError as e:
    if 'already' not in str(e):
        raise e


class SpinalCord(object):
    """
    Generic spinal cord circuitry. Supports muscles, muscle groups and antagonistic relationship between groups.
    """

    def __init__(self, flags, muscles, groups=[], antago=[]):
        """
        Creates the spinal cord with all populations and connections.

        :param flags: Dictionary with generic parameters.
            {
                'excitatory_II': (boolean) create II interneurons and related connections
                'homonymus': (boolean) connections between Ia afferents and motoneurons of the same muscle
                'heteronymus': (boolean) connections between Ia afferents and motoneurons of the same group
                'polysynaptic': (boolean) inhibitory connections from Ia afferents to motoneurons of antagonistic muscles
                ['scale': (real) scale factor of the model]
                ['descending': (boolean) create Poisson generators as descending stimuli]
            }
        :param muscles: list of dictionaries with description of muscles
            {
                'name': (string)
                'mn_params': (dictionary) to be sent to motorpool.MotorPool
                'Ia_params': (dictionary) for muscle_spindle NEST model ({'primary': True})
                'II_params': (dictionary) for muscle_spindle NEST model ({'primary': False})
                'motoneurons': number of motoneurons
                'spindles': number of muscle spindles
                ['bundles': (list of strings) names of the bundles]
                ['homonymus_rule': rule for homonymus connections]
                ['homonymus_weight': synaptic weight for homonymus connections]
            }
        :param groups: list of dictionaries describing muscle groups
            {
                'name': (string)
                'muscles': (list of string) list of muscle names, these should be included in the previous parameter
                'n_II_interneurons': number of II interneurons for the group
                ['heteronymus_rule': rule for heteronymus connections]
                ['eteronymus_weight': synaptic weight for heteronymus connections]
                ['excitatory_II_rule': rule for excitatory-II connections]
                ['excitatory_II_weight': synaptic weight for excitatory-II connections]
            }
        :param antago: list of dictionaries describing antagonistic relationships
            {
                'g1': {'group': 'flexors', 'n_Ia_interneurons': int(196.0/169 * 1571)},
                'g2': {'group': 'extensors', 'n_Ia_interneurons': int(196.0/169 * (774+681))}
                ['excitatory_Ia_rule': rule for polysynaptic excitatory connections]
                ['excitatory_Ia_weight': synaptic weight for polysynaptic excitatory connections]
                ['inhibitory_Ia_rule': rule for polysynaptic inhibitory connections]
                ['inhibitory_Ia_weight': synaptic weight for polysynaptic inhibitory connections]
            }
        """
        # store parameters
        self._scale = flags.get('scale', 1.0)
        self._stdw = 1.0/self._scale
        self._excitatory_II = flags['excitatory_II']
        self._homonymus = flags['homonymus']
        self._heteronymus = flags['heteronymus']
        self._polysynaptic = flags['polysynaptic']
        self._descending = flags.get('descending', False)

        # store muscle dict
        self._muscles = dict()
        for m in muscles:
            _m = dict()
            _m['mn_params'] = m['mn_params']
            _m['Ia_params'] = m['Ia_params']
            _m['II_params'] = m['II_params']
            _m['n_mn'] = m['motoneurons']
            _m['n_ms'] = m['spindles']
            _m['bundles'] = m.get('bundles', [''])
            _m['mp'] = []
            _m['Ia'] = []
            _m['II'] = []
            _m['alpha'] = None
            _m['gamma_dyn'] = None
            _m['gamma_st'] = None
            _m['homonymus_rule'] = m.get('homonymus_rule', 'all_to_all')
            _m['homonymus_weight'] = m.get('homonymus_weight', 1.0) / self._scale
            self._muscles[m['name']] = _m

        # store groups
        self._groups = dict()
        for g in groups:
            _g = dict()
            _g['muscles'] = g['muscles']
            _g['n_II_interneurons'] = g['n_II_interneurons']
            _g['interII'] = None
            if len(g['muscles']) == 0:
                raise RuntimeError('Muscle groups cannot be empty.')
            # check if the muscle is there
            for m in g['muscles']:
                if m not in self._muscles:
                    raise RuntimeError('Muscle ' + m + ' does not exist.')
            _g['heteronymus_rule'] = g.get('heteronymus_rule', {'rule': 'pairwise_bernoulli', 'p': 0.6})
            _g['heteronymus_weight'] = g.get('heteronymus_weight', 1.0) / self._scale
            _g['excitatory_II_rule'] = g.get('excitatory_II_rule', 'all_to_all')
            _g['excitatory_II_weight'] = g.get('excitatory_II_weight', 1.0) / self._scale
            self._groups[g['name']] = _g

        # store antagonistic relations between groups
        self._antago = []
        for a in antago:
            _a = dict()
            if not ('g1' in a and 'g2' in a):
                raise RuntimeError('Antagonistic relationship should specify two muscle groups.')
            _a['g1'] = a['g1']
            _a['g2'] = a['g2']
            # check groups
            if _a['g1']['group'] not in self._groups:
                raise RuntimeError('Muscle group ' + _a['g1']['group'] + ' does not exist.')
            if _a['g2']['group'] not in self._groups:
                raise RuntimeError('Muscle group ' + _a['g1']['group'] + ' does not exist.')
            _a['g1_interIa'] = None
            _a['g2_interIa'] = None
            _a['excitatory_Ia_rule'] = a.get('excitatory_Ia_rule', 'all_to_all')
            _a['excitatory_Ia_weight'] = a.get('excitatory_Ia_weight', 1.0) / self._scale
            _a['inhibitory_Ia_rule'] = a.get('inhibitory_Ia_rule', 'all_to_all')
            _a['inhibitory_Ia_weight'] = a.get('inhibitory_Ia_weight', 1.0) / self._scale
            self._antago.append(_a)

        # create everything
        for m in self._muscles:
            self._create_muscle(self._muscles[m])
        for g in self._groups:
            self._connect_group(self._groups[g])
        for a in self._antago:
            self._connect_antago(a)

    def _create_muscle(self, muscle):

        # descending signals
        if self._descending:
            muscle['alpha'] = nest.Create('poisson_generator', 1, {'rate': 0.0})
            muscle['gamma_dyn'] = nest.Create('poisson_generator', 1, {'rate': 0.0})
            muscle['gamma_st'] = nest.Create('poisson_generator', 1, {'rate': 0.0})

        nb = len(muscle['bundles'])
        for b in muscle['bundles']:

            # motorpool
            mp = MotorPool(int(self._scale * muscle['n_mn'] / nb), muscle['mn_params'])
            muscle['mp'].append(mp)

            # spindles
            Ia = nest.Create("muscle_spindle", int(self._scale * muscle['n_ms'] / nb))
            nest.SetStatus(Ia, muscle['Ia_params'])
            muscle['Ia'].append(Ia)
            II = nest.Create("muscle_spindle", int(self._scale * muscle['n_ms'] / nb))
            nest.SetStatus(II, muscle['II_params'])
            muscle['II'].append(II)

            if self._descending:
                nest.Connect(muscle['alpha'], mp.motoneurons, 'all_to_all', {'weight': 1.0, 'delay': 1.0})
                nest.Connect(muscle['gamma_dyn'], Ia, 'all_to_all', {'weight': 1.0, 'delay': 1.0, 'receptor_type': 1})
                nest.Connect(muscle['gamma_st'], Ia, 'all_to_all', {'weight': 1.0, 'delay': 1.0, 'receptor_type': 2})
                nest.Connect(muscle['gamma_dyn'], II, 'all_to_all', {'weight': 1.0, 'delay': 1.0, 'receptor_type': 1})
                nest.Connect(muscle['gamma_st'], II, 'all_to_all', {'weight': 1.0, 'delay': 1.0, 'receptor_type': 2})

            if self._homonymus:
                nest.Connect(Ia, mp.motoneurons,
                             muscle['homonymus_rule'],
                             {'weight': muscle['homonymus_weight'], 'delay': 1.0})

    def _connect_group(self, group):

        mn = self.get_group_mn(group)
        Ia = self.get_group_Ia(group)
        II = self.get_group_II(group)

        # heteronymus Ia
        if self._heteronymus:
            heteronymus_rule = {'rule': 'pairwise_bernoulli', 'p': 0.6}
            heteronymus_params = {'weight': 0.6 * self._stdw, 'delay': 1.0}

            for i in range(len(mn)):
                for j in range(len(mn)):
                    if i != j:
                        nest.Connect(Ia[i], mn[j], heteronymus_rule, heteronymus_params)

        # II excitatory
        if self._excitatory_II:

            IIinter = nest.Create('iaf_psc_alpha', int(self._scale * group['n_II_interneurons']))

            for i in range(len(mn)):
                nest.Connect(II[i], IIinter,
                             group['excitatory_II_rule'],
                             {'weight': group['excitatory_II_weight'], 'delay': 1.0})
                nest.Connect(IIinter, mn[i],
                             group['excitatory_II_rule'],
                             {'weight': group['excitatory_II_weight'], 'delay': 1.0})

            group['interII'] = IIinter

    def _connect_antago(self, antago):

        if self._polysynaptic:
            # excitatory_Ia_rule = 'all_to_all'
            excitatory_Ia_rule = antago['excitatory_Ia_rule']
            # excitatory_Ia_params = {'weight': self._stdw, 'delay': 1.0}
            excitatory_Ia_params = {'weight': antago['excitatory_Ia_weight'], 'delay': 1.0}
            # inhibitory_Ia_rule = 'all_to_all'
            inhibitory_Ia_rule = antago['inhibitory_Ia_rule']
            # inhibitory_Ia_params = {'weight': -self._stdw, 'delay': 1.0}
            inhibitory_Ia_params = {'weight': antago['inhibitory_Ia_weight'], 'delay': 1.0}

            a = antago['g1']['group']
            b = antago['g2']['group']

            Iainter_a2b = nest.Create('iaf_psc_alpha', int(self._scale * antago['g1']['n_Ia_interneurons']))
            Iainter_b2a = nest.Create('iaf_psc_alpha', int(self._scale * antago['g2']['n_Ia_interneurons']))

            for m in self._groups[a]['muscles']:
                _m = self._muscles[m]
                if self._descending:
                    nest.Connect(_m['alpha'], Iainter_a2b, excitatory_Ia_rule, excitatory_Ia_params)
                for Ia in _m['Ia']:
                    nest.Connect(Ia, Iainter_a2b, excitatory_Ia_rule, excitatory_Ia_params)
                for mp in _m['mp']:
                    nest.Connect(Iainter_b2a, mp.motoneurons, inhibitory_Ia_rule, inhibitory_Ia_params)

            for m in self._groups[b]['muscles']:
                _m = self._muscles[m]
                if self._descending:
                    nest.Connect(_m['alpha'], Iainter_b2a, excitatory_Ia_rule, excitatory_Ia_params)
                for Ia in _m['Ia']:
                    nest.Connect(Ia, Iainter_b2a, excitatory_Ia_rule, excitatory_Ia_params)
                for mp in _m['mp']:
                    nest.Connect(Iainter_a2b, mp.motoneurons, inhibitory_Ia_rule, inhibitory_Ia_params)

            antago['g1_interIa'] = Iainter_a2b
            antago['g2_interIa'] = Iainter_b2a

    def get_group_mn(self, group):
        """
        Retrieves all GIDs of motoneurons for a group
        :param group: name of the group
        :return: list of GIDs
        """

        ret = []
        for m in group['muscles']:
            _m = self._muscles[m]
            for b in range(len(_m['bundles'])):
                ret.append(_m['mp'][b].motoneurons)
        return ret

    def get_group_Ia(self, group):
        """
        Retrieves all GIDs of Ia afferents for a group
        :param group: name of the group
        :return: list of GIDs
        """

        ret = []
        for m in group['muscles']:
            _m = self._muscles[m]
            for b in range(len(_m['bundles'])):
                ret.append(_m['Ia'][b])
        return ret

    def get_group_II(self, group):
        """
        Retrieves all GIDs of II afferents for a group
        :param group: name of the group
        :return: list of GIDs
        """
        ret = []
        for m in group['muscles']:
            _m = self._muscles[m]
            for b in range(len(_m['bundles'])):
                ret.append(_m['II'][b])
        return ret

    def print_all(self):
        """
        Prints spinal cord configuration
        """

        print("Muscles:")
        for m in self._muscles:
            print("%s: %r" % (m, self._muscles[m]))

        print()
        print("Groups:")
        for g in self._groups:
            print("%s: %r" % (g, self._groups[g]))

        print()
        print("Antagonistic relationships:")
        print(self._antago)

    def get_all_bundles(self):
        """
        Retrieves a list of all bundles
        :return: list of bundle names
        """
        ret = []
        for m in self._muscles:
            for b in self._muscles[m]['bundles']:
                if not b == '':
                    ret.append(m+"_"+b)
                else:
                    ret.append(m)
        return ret

    def get_IO_populations(self):
        """
        Returns a dictionary with all input or ouput populations. These include twitches_integrators, muscle spindles
        and descending inputs (if present).
        :return:
        """
        ret = dict()
        for m in self._muscles:

            if self._descending:
                ret[m + '_descending_alpha'] = self._muscles[m]['alpha']
                ret[m + '_descending_gamma_dyn'] = self._muscles[m]['gamma_dyn']
                ret[m + '_descending_gamma_st'] = self._muscles[m]['gamma_st']

            bun = self._muscles[m]['bundles']
            mp = self._muscles[m]['mp']
            Ia = self._muscles[m]['Ia']
            II = self._muscles[m]['II']
            for i in range(len(bun)):
                name = m
                if not bun[i] == '':
                    name = name + "_" + bun[i]
                ret[name + '_m'] = mp[i].muscle
                ret[name + '_Ia'] = Ia[i]
                ret[name + '_II'] = II[i]
        return ret

    def _get_all_gids_muscle(self, muscle):

        mus = self._muscles[muscle]

        ret = ()
        if self._descending:
            ret = ret + mus['alpha']
            ret = ret + mus['gamma_dyn']
            ret = ret + mus['gamma_st']
        for i in range(len(mus['bundles'])):
            ret = ret + mus['mp'][i].get_all_gids()
            ret = ret + mus['Ia'][i]
            ret = ret + mus['II'][i]

        return ret

    def get_all_gids(self):
        """
        Return all GIDS of all nodes created
        :return: tuple of GIDs
        """
        ret = ()

        for m in self._muscles:
            ret = ret + self._get_all_gids_muscle(m)

        if self._excitatory_II:
            for g in self._groups:
                ret = ret + self._groups[g]['interII']

        if self._polysynaptic:
            for a in self._antago:
                ret = ret + a['g1_interIa']
                ret = ret + a['g2_interIa']

        return ret

    def print_nrp_populations(self):
        """
        Print IO populations so that the output can be put inside the bibi file of an experiment of the NRP.
        """

        pops = self.get_IO_populations()

        for p in pops:
            l = pops[p]
            fr = min(l)-1
            to = max(l)
            print('    <populations population="%s" xsi:type="Range" from="%d" to="%d"/>' % (p, fr, to))

    def get_all_spindles_gids(self):
        """
        Retrieves GIDs for all muscle spindles
        :return: list of GIDs
        """

        ret = ()
        for m in self._muscles:
            mus = self._muscles[m]
            for i in range(len(mus['bundles'])):
                ret = ret + mus['Ia'][i]
                ret = ret + mus['II'][i]
        return ret

    def get_all_motoneurons_gids(self):
        """
        Retrieves GIDs for all motoneurons
        :return: list of GIDs
        """

        ret = ()
        for m in self._muscles:
            mus = self._muscles[m]
            for i in range(len(mus['bundles'])):
                ret = ret + mus['mp'][i].motoneurons
        return ret

    def get_all_II_inter_gids(self):
        """
        Retrieves GIDs for all II-interneurons
        :return: list of GIDs
        """

        ret = ()
        if self._excitatory_II:
            for g in self._groups:
                ret = ret + self._groups[g]['interII']
        return ret

    def get_all_Ia_inter_gids(self):
        """
        Retrieves GIDs for all Ia-interneurons
        :return: list of GIDs
        """

        ret = ()
        if self._polysynaptic:
            for a in self._antago:
                ret = ret + a['g1_interIa']
                ret = ret + a['g2_interIa']
        return ret

    def get_all_recordable_gids(self):
        """
        Retrieves GIDs for all units from which it is possibile to record spikes.
        :return: list of GIDs
        """
        return self.get_all_spindles_gids() + self.get_all_motoneurons_gids() + self.get_all_II_inter_gids() + self.get_all_Ia_inter_gids()

    def _find_group(self, muscle):
        for g in self._groups:
            if muscle in self._groups[g]['muscles']:
                return g
        return None

    def attach_descending(self, connections):
        """
        Attach descending stimuli to spinal cord populations.

        :param connections: dictionary specifying the connections to be made for each muscle
            {'muscle': {'alpha': (gids)[, 'alpha_conn_spec': conn_spec, 'alpha_syn_spec': syn_spec,
                        'gamma_dyn': (gids), 'gamma_dyn_conn_spec': conn_spec, 'gamma_dyn_syn_spec': syn_spec,
                        'gamma_st': (gids), 'gamma_st_conn_spec': conn_spec, 'gamma_st_syn_spec': syn_spec]}}
        """
        if self._descending:
            print("WARNING: attaching descending inputs to a network that already has descending stimuli.")
        for m, pars in connections.items():
            for b in self._muscles[m]['mp']:
                nest.Connect(pars['alpha'], b.motoneurons,
                             pars.get('alpha_conn_spec', 'all_to_all'),
                             pars.get('alpha_syn_spec', {'weight': 1.0, 'delay': 1.0}))
            for b in self._muscles[m]['Ia']:
                if 'gamma_dyn' in pars:
                    nest.Connect(pars['gamma_dyn'], b,
                                 pars.get('gamma_dyn_conn_spec', 'all_to_all'),
                                 pars.get('gamma_dyn_syn_spec', {'weight': 1.0, 'delay': 1.0, 'receptor_type': 1}))
                if 'gamma_st' in pars:
                    nest.Connect(pars['gamma_st'], b,
                                 pars.get('gamma_st_conn_spec', 'all_to_all'),
                                 pars.get('gamma_st_syn_spec', {'weight': 1.0, 'delay': 1.0, 'receptor_type': 2}))
            for b in self._muscles[m]['II']:
                if 'gamma_dyn' in pars:
                    nest.Connect(pars['gamma_dyn'], b,
                                 pars.get('gamma_dyn_conn_spec', 'all_to_all'),
                                 pars.get('gamma_dyn_syn_spec', {'weight': 1.0, 'delay': 1.0, 'receptor_type': 1}))
                if 'gamma_st' in pars:
                    nest.Connect(pars['gamma_st'], b,
                                 pars.get('gamma_st_conn_spec', 'all_to_all'),
                                 pars.get('gamma_st_syn_spec', {'weight': 1.0, 'delay': 1.0, 'receptor_type': 2}))
            if self._polysynaptic:
                g = self._find_group(m)
                for a in self._antago:
                    if g == a['g1']['group']:
                        nest.Connect(pars['alpha'], a['g1_interIa'],
                                     a['excitatory_Ia_rule'],
                                     {'weight': a['excitatory_Ia_weight'], 'delay': 1.0})
                    if g == a['g2']['group']:
                        nest.Connect(pars['alpha'], a['g2_interIa'],
                                     a['excitatory_Ia_rule'],
                                     {'weight': a['excitatory_Ia_weight'], 'delay': 1.0})

    def get_populations_dictionary(self, recordableonly=False, minmaxonly=False):
        """
        Retrieves a dictionary with all neuronal populations

        :return: {'pop': (gids)}
        """
        ret = {}

        # muscles
        for m in self._muscles:

            bun = self._muscles[m]['bundles']
            mp = self._muscles[m]['mp']
            Ia = self._muscles[m]['Ia']
            II = self._muscles[m]['II']
            for i in range(len(bun)):
                name = m
                if not bun[i] == '':
                    name = name + "_" + bun[i]
                ret[name + '_mn'] = mp[i].motoneurons
                if not recordableonly:
                    ret[name + '_m'] = mp[i].muscle
                ret[name + '_Ia'] = Ia[i]
                ret[name + '_II'] = II[i]

        if self._excitatory_II:
            for g in self._groups:
                ret[g + '_interII'] = self._groups[g]['interII']

        if self._polysynaptic:
            for a in self._antago:
                ret[a['g1']['group'] + '_interIa'] = a['g1_interIa']
                ret[a['g2']['group'] + '_interIa'] = a['g2_interIa']

        if minmaxonly:
            for p in ret:
                ret[p] = (min(ret[p]), max(ret[p]))

        return ret
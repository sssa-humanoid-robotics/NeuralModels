import nest
import math

# NEST modules can only be loaded once
try:
    nest.Install("actuators_module")
except nest.NESTError as e:
    if 'already' not in str(e):
        raise e


class MotorPool(object):
    """
    Implements an alpha-motoneuron pool and their connection with a twitches integrator.

    For this class to work, actuators_module have to be installed in NEST.
    """

    def __init__(self, n, params):
        """
        Creates the pool and all the connections.

        :param n: number of motoneurons
        :param params: dictionary of parameters, it should include the following entries:
            {
                'dmn',
                'dmx',
                'DSF',
                'cspf',
                'taumax',
                'tauadj',
                'tauslp',
                'pmx',
                'pmn',
                'FSF',
                'smn',
                'ssl',
                'TSF',
                'Ncal'
            }
        """
        self._motoneurons = nest.Create('iaf_psc_alpha', n)
        for i in range(n):
            D = (params['dmx'] - params['dmn']*math.log(params['Ncal'] - params['Ncal']/n*i))*params['DSF']
            C = math.pi*D*D*params['cspf']
            tau = params['taumax'] - (D - params['tauadj'])*params['tauslp']
            nest.SetStatus([self._motoneurons[i]], {'C_m': C*1e12, 'tau_m': tau*1000})

        p = [(params['pmx']-params['pmn']*math.log(params['Ncal'] - params['Ncal']/n*x))*params['FSF'] for x in range(n)]
        Ti = [(params['smn']-params['ssl']*x/n)*params['TSF'] + params['smn'] for x in range(n)]
        self._muscle = nest.Create('twitches_integrator', 1)
        nest.SetStatus(self._muscle, {'p': p, 'Ti': Ti})

        for i in range(n):
            nest.Connect([self._motoneurons[i]], self._muscle, 'one_to_one', {'receptor_type': i + 1})

    @property
    def motoneurons(self):
        """
        :return: GIDs for motoneurons
        """
        return self._motoneurons

    @property
    def muscle(self):
        """
        :return: GID for twitches integrator
        """
        return self._muscle

    def get_all_gids(self):
        """
        :return: all GIDs of nodes created by the class
        """
        return self.motoneurons + self.muscle

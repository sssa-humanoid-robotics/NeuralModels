# Functions that operate with a collection of spikes
# i.e. a list of (id, time)
import nest
import math
import matplotlib.pyplot as plt
import numpy as np


def import_spikes_from_file(filename, skiplines=0):
    """
    Import spikes from a file. The file should specify one spike per line, in the form of "GID time"
    :param filename: name of the file
    :param skiplines: initial lines to be skipped
    :return: spikes read from the file
    """
    spikes = []
    skip = 0
    with open(filename) as f:
        for line in f:
            if skip < skiplines:
                skip = skip + 1
                continue
            idt = line.split(' ')
            spikes.append((int(idt[0]), float(idt[1])))
    return spikes


def save_spikes_to_file(spikes, filename):
    """
    Save spikes to a file, in the form of "GID time", one line per spike
    :param spikes: spikes to be saved
    :param filename: filename to be saved
    """
    with open(filename, 'wt') as f:
        f.write('ID time\n')
        for sp in spikes:
            f.write('%d %f\n' % sp)


def get_gids(spikes):
    """
    Returns the list of GIDs of the spikes
    :param spikes: list of spikes
    :return: GIDs list
    """
    gids = []
    for sp in spikes:
        if not sp[0] in gids:
            gids.append(sp[0])
    return sorted(gids)


def remap_gids(spikes, mapping=None, orig=None, dest=None):
    """
    Change the GIDs of the spikes according to a mapping.
    The mapping can be expressed either as a dictionary or as list of original and new GIDs.

    :param spikes: list of spikes
    :param mapping: dictionary {oldgid: newgid}
    :param orig: list of original gids
    :param dest: list of new gids
    :return: list of spikes with remapped GIDs
    """
    if mapping is None:
        if dest is None:
            return spikes
        if orig is None:
            orig = get_gids(spikes)
        if not len(orig) == len(dest):
            raise Exception('Wrong spike remapping')
        mapping = dict(zip(orig, dest))
    newspikes = []
    for sp in spikes:
        newspikes.append((mapping[sp[0]], sp[1]))
    return newspikes


def sort_by_gids(spikes):
    """
    Sort the spikes by gid.

    :param spikes: unsorted spikes
    :return: a dict of numpy arrays of times, with GIDs as keys
    """
    gids = get_gids(spikes)
    spsorted = dict(zip(gids, [list() for _ in range(len(gids))]))
    for sp in spikes:
        spsorted[sp[0]].append(sp[1])
    for gid in spsorted:
        spsorted[gid] = np.array(spsorted[gid])
    return spsorted


def scale_times(spikes, mul, add=0):
    """
    Change times of all spikes to mul * t + add .

    :param spikes: list of spikes
    :param mul: multiplicative factor
    :param add: additive factor
    :return: list of spikes with times changed
    """
    scaled = []
    for sp in spikes:
        scaled.append((sp[0], sp[1] * mul + add))
    return scaled


def get_spikes_from_detector(detector):
    """
    Retrieves a list of spikes from a spike_detector node.

    :param detector: the spike_detector
    :return: list of spikes retrieved
    """
    events = nest.GetStatus(detector, 'events')[0]
    return list(zip(events['senders'], events['times']))


def trim_to_resolution(spikes):
    """
    Trim the times of the spikes to match the simulator resolution.
    This is used to avoid exceptions risen by the NEST kernel.

    :param spikes: list of spikes
    :return: list of spikes, with times trimmed
    """
    trimmed = []
    res = nest.GetKernelStatus('resolution')
    dec = int(abs(math.log10(res)))
    trimstr = '%.' + str(dec) + 'f'
    for sp in spikes:
        trimmed.append((sp[0], float(trimstr % (sp[1]))))  # ouch
    return trimmed


def trim_to_resolution_sorted(spikes):
    """
    Trim the times of the spikes to match the simulator resolution.
    This is used to avoid exceptions risen by the NEST kernel.

    :param spikes: dictionary of spikes sorted by GIDs
    :return: list of spikes, with times trimmed
    """
    res = nest.GetKernelStatus('resolution')
    dec = int(abs(math.log10(res)))
    for gid in spikes:
        spikes[gid] = np.round(spikes[gid], dec)
    return spikes

def plot_spikes(spikes, labels=None, filename=None, fontsize=8):
    """
    Plots a raster plot of the spikes and optionally save it to file.
    Populations can be labelled.

    :param spikes: the spikes to be plotted
    :param labels: (optional) dictionary of labels for the populations: {'pop': (minGID, maxGID)}
    :param filename: (optional) path of the image to be saved
    """
    gids = []
    times = []
    for sp in spikes:
        gids.append(sp[0])
        times.append(sp[1] / 1000.0)
    plt.scatter(times, gids, s=1)
    # plt.xlabel('time (s)')
    plt.ylabel('neuron ID')
    if labels is not None:
        axes = plt.gca()
        ylim = axes.get_ylim()
        minid = ylim[0]
        maxid = ylim[1]
        for l in labels:
            lab = labels[l]
            nmin = float(lab[0] - minid) / (maxid - minid)
            nmax = float(lab[1] - minid) / (maxid - minid)
            plt.text(1.0, (nmax+nmin) / 2.0, l, transform=plt.gca().transAxes, fontsize=fontsize)
    # plt.show()
    if filename is not None:
        plt.gcf().set_size_inches(10, 6)
        plt.savefig(filename, dpi=300)

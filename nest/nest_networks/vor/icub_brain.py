# -*- coding: utf-8 -*-
# pragma: no cover
__author__ = 'Mattia Villani'

from hbp_nrp_cle.brainsim import simulator as sim
import numpy as np 
import math
import pyNN
from pyNN.standardmodels import build_translations, StandardSynapseType
from pyNN.models import BaseCellType
from pyNN.nest import native_cell_type, native_synapse_type, nest
from pyNN.nest.cells import get_defaults
from pyNN.parameters import Sequence
from pyNN.utility import get_simulator, init_logging, normalized_filename
import pyNN.utility.plotting as plotting
from itertools import repeat
import logging
import json

logger = logging.getLogger(__name__)

sim.setup()

simulator = sim
get_activations = None
update_hv = None
get_rates = None

def createBrain():
    global get_activations
    global update_hv
    global get_rates
    sim.setup(timestep=0.1, min_delay=0.1, max_delay=10.0)
    param_muscles = {
        'dmn': 8.5,
        'dmx': 60,  # 120.0,
        'DSF': 1e-6/120.0*109.37,
        'cspf': 1e-2,
        'taumax': 2.0e-3,  # 12.5e-3,
        'tauadj': 60e-6,
        'tauslp': 9.44e1,
        'pmx': 15.0,
        'pmn': 2.25,
        'FSF': 1.0/15.0*18.19,
        'smn': 0.104,
        'ssl': 0.207,
        'TSF': 0.6892,
        'Ncal': 740
    } 
    UNITS_MAP = {
        'spikes': 'ms',
        'V_m': 'mV',
        'I_syn': 'nA',
        'V_th':'mV',
        'hv':'deg/ms',
        'activation':'%',
        'debug':'unit'
    }
    try:
        nest.Install('muscle_module')
    except: pass
    try:
        nest.Install("vormodule")
    except : pass
    try:
        pass#nest.SetKernelStatus({"resolution": 0.01})
    except : pass
    classReg = native_cell_type("vor_regular")
    classIrr = native_cell_type("vor_irregular")
    Muscle_class_as_native = native_cell_type("muscle_model")
    class muscle_class( Muscle_class_as_native ):
        def __init__(self,**parameters):
            super(muscle_class,self).__init__( **parameters)
            self.standard_receptor_type = False 
            self.default_parameters['p'] = Sequence(np.array([]))
            self.default_parameters['Ti']= Sequence(np.array([]))
    class MCProjection( sim.Projection ):
        def _convergent_connect(self, presynaptic_indices, postsynaptic_index,
                                **connection_parameters):
            postsynaptic_cell = self.post[postsynaptic_index]
            if ( not isinstance( postsynaptic_cell.celltype, muscle_class) ):
                super(MCProjection, self)._convergent_connect(presynaptic_indices, postsynaptic_index,
                                **connection_parameters)
            presynaptic_cells = self.pre.all_cells[presynaptic_indices]
            assert presynaptic_cells.size == presynaptic_indices.size
            assert len(presynaptic_cells) > 0, presynaptic_cells
            weights = connection_parameters.pop('weight')
            delays = connection_parameters.pop('delay')
            port = connection_parameters.pop('receptor_type')
            assert not( postsynaptic_cell.celltype.standard_receptor_type )
            if np.isscalar(weights): weights = repeat(weights)
            if np.isscalar(delays): delays = repeat(delays)
            if np.isscalar(port): port = repeat(port)
            for pre, w, d, p in zip(presynaptic_cells, weights, delays, port):
                args = {'receptor_type': p } 
                nest.Connect([pre], [postsynaptic_cell],'one_to_one',args)
    class PortedStaticSynapse ( sim.StaticSynapse ):
        translations = sim.StaticSynapse.translations.copy()
        translations['port'] = build_translations(('port', 'receptor_type'))['port']
        default_parameters = sim.StaticSynapse.default_parameters.copy()
        default_parameters['port'] = 0
        def __init__(self, port=1):
            super( PortedStaticSynapse, self ).__init__()
            self.parameter_space.update( **({'port':port}) )
    muscle_class.units = dict(((var, UNITS_MAP.get(var, 'unknown')) for var in muscle_class.recordable))

    N = 50

    #horizontal
    rlhaff = sim.Population( int(N*1.2), classReg, {},  label="horizontal afferents")
    rrhaff = sim.Population( int(N*1.2), classReg, {},  label="horizontal afferents")
    ilhaff = sim.Population( N, classIrr, {},  label="horizontal afferents")
    irhaff = sim.Population( N, classIrr, {},  label="horizontal afferents")
    lhaff = rlhaff + ilhaff
    rhaff = rrhaff + irhaff
    hafferents = lhaff + rhaff

    #anterior
    rlaaff = sim.Population( int(N*1.2), classReg, {},  label="anterior afferents")
    #rraaff = sim.Population( int(N*1.2), classReg, {},  label="anterior afferents")
    ilaaff = sim.Population( N, classIrr, {},  label="anterior afferents")
    #iraaff = sim.Population( N, classIrr, {},  label="anterior afferents")
    laaff = rlaaff + ilaaff
    #raaff = rraaff + iraaff
    aafferents = laaff# + raaff

    #posterior
    #rlpaff = sim.Population( int(N*1.2), classReg, {},  label="posterior afferents")
    rrpaff = sim.Population( int(N*1.2), classReg, {},  label="posterior afferents")
    #ilpaff = sim.Population( N, classIrr, {},  label="posterior afferents")
    irpaff = sim.Population( N, classIrr, {},  label="posterior afferents")
    #lpaff = rlpaff + ilpaff
    rpaff = rrpaff + irpaff
    pafferents = rpaff #+ lpaff

    afferents = hafferents + aafferents + pafferents
    #afferents.record(('spikes'))

    lhvn = sim.Population( N, sim.IF_curr_alpha(), label="lhvn")
    rhvn = sim.Population( N, sim.IF_curr_alpha(), label="rhvn")

    lavn = sim.Population( N, sim.IF_curr_alpha(), label="lavn")
    #ravn = sim.Population( N, sim.IF_curr_alpha(), label="ravn")
    #lpvn = sim.Population( N, sim.IF_curr_alpha(), label="lpvn")
    rpvn = sim.Population( N, sim.IF_curr_alpha(), label="rpvn")

    nin = sim.Population( N, sim.IF_curr_alpha(), label="in")
    nln = sim.Population( N, sim.IF_curr_alpha(), label="ln")
    rmr = sim.Population( N, sim.IF_curr_alpha(), label="rmr")
    lmr = sim.Population( N, sim.IF_curr_alpha(), label="lmr")

    nsr = sim.Population( N, sim.IF_curr_alpha(), label="lsr")
    nir = sim.Population( N, sim.IF_curr_alpha(), label="lir")
    #nSr = sim.Population( N, sim.IF_curr_alpha(), label="lsr")
    #nIr = sim.Population( N, sim.IF_curr_alpha(), label="lir")

    def getPTi(n,pr = param_muscles):
        p = [(pr['pmx']-pr['pmn']*math.log(pr['Ncal'] - pr['Ncal']/n*x))*pr['FSF'] for x in xrange(n)]
        Ti = [(pr['smn']-pr['ssl']*x/n)*pr['TSF'] + pr['smn'] for x in xrange(n)]
        return {'p': p, 'Ti': Ti}         
    muscles = sim.Population( 6, muscle_class, {}, label='muscles') 
    muscles.set( ** getPTi(N) )  
    muscles.record(('activation'))
    
    mll = muscles[0:1]
    mlm = muscles[1:2]
    mrm = muscles[2:3]
    mrl = muscles[3:4]

    ms = muscles[4:5]
    mi = muscles[5:6]
    #mS = muscles[6:7]
    #mI = muscles[7:8]

    #connection
    p = 0.25
    co2o = sim.OneToOneConnector()
    cfpc = sim.FixedProbabilityConnector(p, allow_self_connections=False)
    cfpcs= sim.FixedProbabilityConnector(math.sqrt(p), allow_self_connections=False)
    sts = sim.StaticSynapse(weight=1.0, delay=0.1) 

    sim.Projection( lhaff, lhvn, cfpcs,synapse_type = sts )
    sim.Projection( rhaff, rhvn, cfpcs, synapse_type = sts )
    sim.Projection( laaff, lavn, cfpcs,synapse_type = sts )
    #sim.Projection( raaff, ravn, cfpcs, synapse_type = sts )
    #sim.Projection( lpaff, lpvn, cfpcs,synapse_type = sts )
    sim.Projection( rpaff, rpvn, cfpcs, synapse_type = sts )

    sim.Projection( lhvn, nin, cfpcs, synapse_type = sts )
    sim.Projection( rhvn, nln, cfpcs, synapse_type = sts )
    sim.Projection( nin, lmr, cfpcs, synapse_type = sts )
    sim.Projection( nln, rmr, cfpcs, synapse_type = sts )
    
    sim.Projection( lavn, nsr, cfpcs, synapse_type = sts )
    #sim.Projection( ravn, nSr, cfpcs, synapse_type = sts )
    #sim.Projection( lpvn, nIr, cfpcs, synapse_type = sts )
    sim.Projection( rpvn, nir, cfpcs, synapse_type = sts )

    for index in xrange(N):
        syntype = PortedStaticSynapse(index + 1)
        MCProjection( nln[index: index+1], mll, co2o, synapse_type = syntype )
        MCProjection( lmr[index: index+1], mlm, co2o, synapse_type = syntype )
        MCProjection( rmr[index: index+1], mrm, co2o, synapse_type = syntype )
        MCProjection( nin[index: index+1], mrl, co2o, synapse_type = syntype )

        MCProjection( nsr[index: index+1], ms, co2o, synapse_type = syntype )
        MCProjection( nir[index: index+1], mi, co2o, synapse_type = syntype )
        #MCProjection( nSr[index: index+1], mS, co2o, synapse_type = syntype )
        #MCProjection( nIr[index: index+1], mI, co2o, synapse_type = syntype )

    PortedStaticSynapse(0)

    def _get_activations(t):
        dt = t - _get_activations.t
        _get_activations.t = t
        def getAct(muscle):
            if logger is not None: logger.info("\tgetting actual value from "+str(muscle))
            val = 0.
            if muscle is not None :
                val = float( muscle.get_data().segments[0].filter(name="activation")[0][-1] )        
            if logger is not None: logger.info("\t\t... it is "+str(val))
            return val
        amll, amlm, amrm, amrl = getAct(mll), getAct(mlm), getAct(mrm), getAct(mrl)
        val_version = (amll + amrm - amlm - amrl)
        ams, ami = getAct(ms), getAct(mi)
        val_tilt = (ams - ami)
        return (dt, val_version, val_tilt, amll, amrm, amlm, amrl, ams, ami, ams, ami)
    _get_activations.t = 0

    get_activations = _get_activations


    def _update_hv(HVyaw, HVpitch, t):
        scale = math.cos(47.0/180*math.pi)
        sim.set( lhaff, **{'HV': HVyaw } )
        sim.set( rhaff, **{'HV':-HVyaw } )

        sim.set( laaff, **{'HV': HVpitch*scale } )
        #sim.set( raaff, **{'HV': HVpitch*scale } )
        #sim.set( lpaff, **{'HV':-HVpitch*scale } ) 
        sim.set( rpaff, **{'HV':-HVpitch*scale } ) 
    
    update_hv = _update_hv
        
    _t = 0.
    spikeSrcs = [ 
        rlhaff, rrhaff, ilhaff, irhaff, 
        rlaaff, rlaaff, ilaaff, ilaaff, #right anterior is duplicated from left
        rrpaff, rrpaff, irpaff, irpaff] # left posterior is duplicated from right
    _previous_count = [0 for _ in spikeSrcs]

    def _get_rates(t):
        t = float(t)
        dt = t - _get_rates.t
        _get_rates.t = t
        
        count = [ ssrc.mean_spike_count() for ssrc in _get_rates.spikeSrcs ]
        dif_spikes = [count[i] - _get_rates.previous_count[i] for i in xrange(len(count))]
        _get_rates.previous_count = count

        if dt <= 0 : 
            return (0,0,0,0, 0,0,0,0, 0,0,0,0) 
        return ( float(spike_count) / dt for spike_count in dif_spikes ) 
        #rh_rl, rh_rr, rh_il, rh_ir, ra_rl, ra_rr, ra_il, ra_ir, rp_rl, rp_rr, rp_il, rp_ir
    _get_rates.t = _t
    _get_rates.spikeSrcs = spikeSrcs
    _get_rates.previous_count = _previous_count 
    get_rates = _get_rates

    return afferents


circuit = createBrain()


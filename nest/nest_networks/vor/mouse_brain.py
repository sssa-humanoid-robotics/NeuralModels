# -*- coding: utf-8 -*-
# pragma: no cover
__author__ = 'Mattia Villani'

from hbp_nrp_cle.brainsim import simulator as sim
import numpy as np 
import math
import pyNN
from pyNN.standardmodels import build_translations, StandardSynapseType
from pyNN.models import BaseCellType
from pyNN.nest import native_cell_type, native_synapse_type, nest
from pyNN.nest.cells import get_defaults
from pyNN.parameters import Sequence
from pyNN.utility import get_simulator, init_logging, normalized_filename
import pyNN.utility.plotting as plotting
from itertools import repeat
import logging
import json

logger = logging.getLogger(__name__)

sim.setup()

simulator = sim
get_activations = None
update_hv = None

def createBrain():
    global get_activations
    global update_hv
    nest.SetKernelStatus({"resolution": 0.01})
    sim.setup(timestep=0.1, min_delay=0.1, max_delay=10.0)
    param_muscles = {
        'dmn': 8.5,
        'dmx': 60,  # 120.0,
        'DSF': 1e-6/120.0*109.37,
        'cspf': 1e-2,
        'taumax': 2.0e-3,  # 12.5e-3,
        'tauadj': 60e-6,
        'tauslp': 9.44e1,
        'pmx': 15.0,
        'pmn': 2.25,
        'FSF': 1.0/15.0*18.19,
        'smn': 0.104,
        'ssl': 0.207,
        'TSF': 0.6892,
        'Ncal': 740
    } 
    UNITS_MAP = {
        'spikes': 'ms',
        'V_m': 'mV',
        'I_syn': 'nA',
        'V_th':'mV',
        'hv':'deg/ms',
        'activation':'%',
        'debug':'unit'
    }
    try:
        nest.Install("vormodule")
        nest.Install('muscle_module')
    except: pass
    Muscle_class_as_native = native_cell_type("muscle_model")
    regClass = native_cell_type("vor_regular")
    irrClass = native_cell_type("vor_irregular")
    class muscle_class( Muscle_class_as_native ):
        def __init__(self,**parameters):
            super(muscle_class,self).__init__( **parameters)
            self.standard_receptor_type = False 
            self.default_parameters['p'] = Sequence(np.array([]))
            self.default_parameters['Ti']= Sequence(np.array([]))
    class MCProjection( sim.Projection ):
        def _convergent_connect(self, presynaptic_indices, postsynaptic_index,
                                **connection_parameters):
            postsynaptic_cell = self.post[postsynaptic_index]
            if ( not isinstance( postsynaptic_cell.celltype, muscle_class) ):
                super(MCProjection, self)._convergent_connect(presynaptic_indices, postsynaptic_index,
                                **connection_parameters)
            presynaptic_cells = self.pre.all_cells[presynaptic_indices]
            assert presynaptic_cells.size == presynaptic_indices.size
            assert len(presynaptic_cells) > 0, presynaptic_cells
            weights = connection_parameters.pop('weight')
            delays = connection_parameters.pop('delay')
            port = connection_parameters.pop('receptor_type')
            assert not( postsynaptic_cell.celltype.standard_receptor_type )
            if np.isscalar(weights): weights = repeat(weights)
            if np.isscalar(delays): delays = repeat(delays)
            if np.isscalar(port): port = repeat(port)
            for pre, w, d, p in zip(presynaptic_cells, weights, delays, port):
                args = {'receptor_type': p } 
                nest.Connect([pre], [postsynaptic_cell],'one_to_one',args)
    class PortedStaticSynapse ( sim.StaticSynapse ):
        translations = sim.StaticSynapse.translations.copy()
        translations['port'] = build_translations(('port', 'receptor_type'))['port']
        default_parameters = sim.StaticSynapse.default_parameters.copy()
        default_parameters['port'] = 0
        def __init__(self, port=1):
            super( PortedStaticSynapse, self ).__init__()
            self.parameter_space.update( **({'port':port}) )
    muscle_class.units = dict(((var, UNITS_MAP.get(var, 'unknown')) for var in muscle_class.recordable))

    afferents = sim.Population( int(100), regClass, {}, label="all afferents")
    regs = afferents[:int(len(afferents)*0.6)]
    irrs = afferents[int(len(afferents)*0.6):]

    sim.set( regs, **{
        "I_bias": 0.78,
        "G_h": 0.003,
        "w_0": 0.015,
        "t_ref": 0.015,
        "delta_w" : 0.001
    })

    sim.set( irrs, **{
        "I_bias": 0.435,
        "G_h": -0.0058,
        "G_a": -0.0001,
        "w_0": 0.018,
        "t_ref": 0.011,
        "std": 0.0015,
        "delta_w" : 0.001
    })

    laff = regs[:len(regs)/2] + irrs[:len(irrs)/2]
    raff = regs[len(regs)/2:] + irrs[len(irrs)/2:]

    lvn = sim.Population( 25, sim.IF_curr_alpha(), label="lvn")
    rvn = sim.Population( 25, sim.IF_curr_alpha(), label="rvn")

    nin = sim.Population( 25, sim.IF_curr_alpha(), label="in")
    nln = sim.Population( 25, sim.IF_curr_alpha(), label="ln")
    rmr = sim.Population( 25, sim.IF_curr_alpha(), label="rmr")
    lmr = sim.Population( 25, sim.IF_curr_alpha(), label="lmr")

    def getPTi(n,pr = param_muscles):
        p = [(pr['pmx']-pr['pmn']*math.log(pr['Ncal'] - pr['Ncal']/n*x))*pr['FSF'] for x in xrange(n)]
        Ti = [(pr['smn']-pr['ssl']*x/n)*pr['TSF'] + pr['smn'] for x in xrange(n)]
        return {'p': p, 'Ti': Ti}         
    muscles = sim.Population( 4, muscle_class, {}, label='muscles') 
    muscles.set( ** getPTi(26) )  
    muscles.record(('activation'))
    
    mll = muscles[0:1]
    mlm = muscles[1:2]
    mrm = muscles[2:3]
    mrl = muscles[3:4]

    #connection
    p = 0.2
    co2o = sim.OneToOneConnector()
    cfpc = sim.FixedProbabilityConnector(p, allow_self_connections=False)
    cfpcs= sim.FixedProbabilityConnector(math.sqrt(p), allow_self_connections=False)
    sts = sim.StaticSynapse(weight=1.0, delay=0.1) 

    sim.Projection( laff, lvn, cfpcs,synapse_type = sts )
    sim.Projection( raff, rvn, cfpcs, synapse_type = sts )
    sim.Projection( lvn, nin, cfpcs, synapse_type = sts )
    sim.Projection( rvn, nln, cfpcs, synapse_type = sts )
    sim.Projection( nin, lmr, cfpc, synapse_type = sts )
    sim.Projection( nln, rmr, cfpc, synapse_type = sts )
    
    for index in xrange(25):
        syntype = PortedStaticSynapse(index + 1)
        MCProjection( nln[index: index+1], mll, co2o, synapse_type = syntype )
        MCProjection( lmr[index: index+1], mlm, co2o, synapse_type = syntype )
        MCProjection( rmr[index: index+1], mrm, co2o, synapse_type = syntype )
        MCProjection( nin[index: index+1], mrl, co2o, synapse_type = syntype )
    PortedStaticSynapse(0)

    def _get_activations():
        def getAct(muscle):
            if logger is not None: logger.info("\tgetting actual value from "+str(muscle))
            val = 0.
            if muscle is not None :
                val = float( muscle.get_data().segments[0].filter(name="activation")[0][-1] )        
            if logger is not None: logger.info("\t\t... it is "+str(val))
            return val
        amll, amlm, amrm, amrl = getAct(mll), getAct(mlm), getAct(mrm), getAct(mrl), 
        val = 5.0 * (amll + amrm - amlm - amrl)
        return (val, amll, amrm, amlm, amrl)
    get_activations = _get_activations

    def _update_hv(HV):
        sim.set( laff, **{'HV': HV } )
        sim.set( raff, **{'HV':-HV } )
    update_hv = _update_hv

    return afferents


circuit = createBrain()


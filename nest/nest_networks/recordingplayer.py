import nest
from .spikes import get_gids, remap_gids, sort_by_gids, trim_to_resolution_sorted
import numpy as np

import time


def multiply_spikes(spikes, mult, mu, sigma):
    """
    Multiply spikes and add gaussian noise.

    :param spikes: dictionary of spikes (ordered by GID)
    :param mult: multiplication factor
    :param mu: noise mean
    :param sigma: noise standard deviation
    :return: dictionary of spikes (ordered by GID), with the new spikes
    """
    n = len(spikes)
    for i in range(n):
        for j in range(int(mult)):
            spikes[i+n*(j+1)] = spikes[i] + np.random.normal(mu, sigma, len(spikes[i]))
            spikes[i + n * (j + 1)].sort()
    for gid in spikes:
        for i in range(len(spikes[gid])):
            if spikes[gid][i] < 0.1:
                spikes[gid][i] = 0.1
            else:
                break
    return spikes


class RecordingPlayer(object):
    """
    This class reproduces a recorded set of spikes through a set of spike generators.
    """

    def __init__(self, spikes, mult=1, mu=0.0, sigma=0.0, n=0):
        """
        Creates and initialize the spike generators

        :param spikes: the spikes to be reproduced
        :param mult: multiply the number of spike generators by this factor.
                     Spikes are replicated and gaussian noise is added to avoid synchronicity.
        :param mu: mean of the gaussian noise
        :param sigma: standard deviation of the gaussian noise
        :param n: number of spike generators (can be deduced automatically)
        """
        self._spikegen = []
        if n == 0:
            n = len(get_gids(spikes))

        spikes = remap_gids(spikes, dest=range(n))
        spsorted = sort_by_gids(spikes)

        if mult > 1:
            spsorted = multiply_spikes(spsorted, mult, mu, sigma)
            spsorted = trim_to_resolution_sorted(spsorted)

        self._spikegen = nest.Create('spike_generator', int(n*mult))
        for i in range(n*mult):
            nest.SetStatus([self._spikegen[i]], {'spike_times': spsorted[i]})

    def get_gids(self):
        """
        Get the GIDs of the spike generators
        :return: list of GIDs
        """
        return self._spikegen

/*
 *  vor_regular.h
 *
 *  Copyright (C) 2018 Mattia Villani
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef VESTIBULAR_REGULAR_H
#define VESTIBULAR_REGULAR_H

// Includes from nestkernel:
#include "archiving_node.h"
#include "connection.h"
#include "event.h"
#include "nest_types.h"
#include "nest_names.h"
#include "ring_buffer.h"
#include "universal_data_logger.h"

#include "gslrandomgen.h"
#include "randomgen.h"
#include "randomdev.h"
#include "normal_randomdev.h"


// Includes from sli:
#include "dictdatum.h"


namespace sensors {

/*
 *
 * IMPLEMENTING MODEL AT : http://www.jneurosci.org/content/27/4/771
 *
 */
class vestibular_regular : public nest::Archiving_Node{

public:
    vestibular_regular();
    vestibular_regular( const vestibular_regular& );
    using Node::handle;
    using Node::handles_test_event;
    nest::port send_test_event( nest::Node&, nest::port, nest::synindex, bool );

    void handle( nest::SpikeEvent& );         //! accept spikes
    void handle( nest::CurrentEvent& );       //! accept input current
    void handle( nest::DataLoggingRequest& ); //! allow recording with multimeter

    nest::port handles_test_event( nest::SpikeEvent&, nest::port );
    nest::port handles_test_event( nest::CurrentEvent&, nest::port );
    nest::port handles_test_event( nest::DataLoggingRequest&, nest::port );

    void get_status( DictionaryDatum& ) const;
    void set_status( const DictionaryDatum& );

private:
    void init_state_( const Node& proto );
    void init_buffers_();
    void calibrate();
    void update( nest::Time const&, const long, const long );

    friend class nest::RecordablesMap< vestibular_regular >;
    friend class nest::UniversalDataLogger< vestibular_regular >;

    struct Parameters_ {
        double HV;      //!< Head velocity, in deg/ms.
        double I_bias;  //!< I bias, in nA.
        double tau_v;   //!< ,in ms.
        double tau_w;   //!< ,in ms.
        double tau_a;   //!< tau_a : cutting off period, in ms.
        double t_ref;   //!< Duration of refractory period, in ms.
        double delta_w; //!< delta_w, in mV.
        double w_0;     //!< w_0, in mV.
        double std;     //!< sigma, in unit
        double G_h;     //!< G_H, in ms/deg.
        double G_a;     //!< G_A, in ms/deg.
        double V_reset; //!< Reset potential of the membrane, in mV.

        Parameters_();
        void get( DictionaryDatum& ) const;
        void set( const DictionaryDatum& );
    };

    struct State_{
        double V_m;      //!< Membrane potential, in mV.
        double V_th;     //!< Dinamic treshold, in mV.
        double I_syn;    //!< Synaptic current, in nA.
        double HV;       //!< Head velocity, in deg/ms.
        double Xa;       //!< 
        long refr_count; //!< Number of steps neuron is still refractory for

        State_( const Parameters_& );
        void get( DictionaryDatum& ) const;
        void set( const DictionaryDatum&, const Parameters_& );
    };

    struct Buffers_{
        Buffers_( vestibular_regular& );
        Buffers_( const Buffers_&, vestibular_regular& );

        nest::RingBuffer spikes;   //!< Buffer incoming spikes through delay, as sum
        nest::RingBuffer currents; //!< Buffer incoming currents through delay,
                                   //!< as sum

        //! Logger for all analog data
        nest::UniversalDataLogger< vestibular_regular > logger_;
    };

    struct Variables_{
        double dt;        //!< time of each step, in ms.
        long t_ref_steps; //!< Duration of refractory period, in steps.
        librandom::NormalRandomDev randomDevice;
    };

    double get_V_m_() const { return S_.V_m;}
    double get_V_th_() const { return S_.V_th;}
    double get_I_syn_() const { return S_.I_syn;}
    double get_HV_() const { return S_.HV;}

    Parameters_ P_; //!< Free parameters.
    State_ S_;      //!< Dynamic state.
    Variables_ V_;  //!< Internal Variables
    Buffers_ B_;    //!< Buffers.

    static nest::RecordablesMap< vestibular_regular > recordablesMap_;

};

inline nest::port vestibular_regular::send_test_event( nest::Node& target, nest::port receptor_type, nest::synindex, bool ){
    nest::SpikeEvent e;
    e.set_sender( *this );
    return target.handles_test_event( e, receptor_type );
}

inline nest::port vestibular_regular::handles_test_event( nest::SpikeEvent&, nest::port receptor_type ){
    if ( receptor_type != 0 ){
        throw nest::UnknownReceptorType( receptor_type, get_name() );
    }
    return 0;
}

inline nest::port vestibular_regular::handles_test_event( nest::CurrentEvent&, nest::port receptor_type ){
    if ( receptor_type != 0 ){
        throw nest::UnknownReceptorType( receptor_type, get_name() );
    }
    return 0;
}

inline nest::port vestibular_regular::handles_test_event( nest::DataLoggingRequest& dlr, nest::port receptor_type ){
    if ( receptor_type != 0 ){
        throw nest::UnknownReceptorType( receptor_type, get_name() );
    }
    return B_.logger_.connect_logging_device( dlr, recordablesMap_ );
}

inline void vestibular_regular::get_status( DictionaryDatum& d ) const {
    P_.get( d );
    S_.get( d );

    Archiving_Node::get_status( d );
    ( *d )[ nest::names::recordables ] = recordablesMap_.get_list();
}

inline void vestibular_regular::set_status( const DictionaryDatum& d ) {
    Parameters_ ptmp = P_; // temporary copy in case of errors
    ptmp.set( d );         // throws if BadProperty
    State_ stmp = S_;      // temporary copy in case of errors
    stmp.set( d, ptmp );   // throws if BadProperty

    Archiving_Node::set_status( d );

    P_ = ptmp;
    S_ = stmp;
}

} // namespace

#endif /* #ifndef VESTIBULAR_REGULAR_H */

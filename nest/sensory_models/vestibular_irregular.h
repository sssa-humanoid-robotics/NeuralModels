/*
 *  vestibular_irregular.h
 *
 *  Copyright (C) 2018 Mattia Villani
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef VESTIBULAR_IRREGULAR_H
#define VESTIBULAR_IRREGULAR_H


#include "vestibular_regular.h"


namespace sensors {
    
class vestibular_irregular : public vestibular_regular {

public:
    vestibular_irregular();
    
};
    
}

#endif

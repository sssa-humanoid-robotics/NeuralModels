/*
 *  vestibular_regular.cpp
 *
 *  Copyright (C) 2018 Mattia Villani
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "vestibular_regular.h"

// C++ includes:
#include <limits>

// Includes from libnestutil:
#include "numerics.h"

// Includes from nestkernel:
#include "exceptions.h"
#include "kernel_manager.h"
#include "universal_data_logger_impl.h"

// Includes from sli:
#include "dict.h"
#include "dictutils.h"
#include "doubledatum.h"
#include "integerdatum.h"
#include "lockptrdatum.h"

#include "sensors_names.h"

namespace sensors{

/* ----------------------------------------------------------------
 * Recordables map
 * ---------------------------------------------------------------- */
nest::RecordablesMap< vestibular_regular > vestibular_regular::recordablesMap_;

}

namespace nest {
template <> void RecordablesMap< sensors::vestibular_regular >::create(){
    insert_( names::V_m,   &sensors::vestibular_regular::get_V_m_ );
    insert_( names::V_th,  &sensors::vestibular_regular::get_V_th_ );
    insert_( names::I_syn, &sensors::vestibular_regular::get_I_syn_ );
    insert_( sensors::names::hv, &sensors::vestibular_regular::get_HV_ );
}
}

namespace sensors {
    /* ----------------------------------------------------------------
     * Default constructors defining default parameters and state
     * ---------------------------------------------------------------- */
    vestibular_regular::Parameters_::Parameters_()
        : HV( 0.0 )           // deg/ms
        , I_bias( 0.0515 )    // nA
        , tau_v( 1.0 )        // ms
        , tau_w( 9.5 )        // ms
        , tau_a( 20. )        // ms
        , t_ref( 1.0 )        // ms
        , delta_w( 0.003 )    // mV
        , w_0( 0.05 )         // mV
        , std( 0.00007 )      // unit
        , G_h( 0.0156 )       // ms/deg
        , G_a( 0.0 )          // ms/deg
        , V_reset( 0.0 )      // mV
    {}

    vestibular_regular::State_::State_( const Parameters_& p )
        : V_m( p.V_reset )
        , V_th( p.w_0 )
        , I_syn( p.I_bias )
        , HV( p.HV )
        , Xa( 0.0 )
        , refr_count( 0 )
    {}

    /* ----------------------------------------------------------------
     * Parameter and state extractions and manipulation functions
     * ---------------------------------------------------------------- */
    void vestibular_regular::Parameters_::get( DictionaryDatum& d ) const {
        ( *d )[ names::HV ] = HV;
        ( *d )[ names::I_bias ] = I_bias;
        ( *d )[ nest::names::tau_v ] = tau_v;
        ( *d )[ nest::names::tau_w ] = tau_w;
        ( *d )[ names::tau_a ] = tau_a;
        ( *d )[ names::delta_w ] = delta_w;
        ( *d )[ nest::names::t_ref ] = t_ref;
        ( *d )[ names::w_0 ] = w_0;
        ( *d )[ nest::names::std ] = std ;
        ( *d )[ names::G_h ] = G_h;
        ( *d )[ names::G_a ] = G_a;  
        ( *d )[ nest::names::V_reset ] = V_reset;
    }

    void vestibular_regular::Parameters_::set( const DictionaryDatum& d ){
        updateValue< double >( d, names::HV, HV );
        updateValue< double >( d, names::I_bias, I_bias );
        updateValue< double >( d, nest::names::tau_v, tau_v );
        updateValue< double >( d, nest::names::tau_w, tau_w );
        updateValue< double >( d, names::tau_a, tau_a );
        updateValue< double >( d, names::delta_w, delta_w );
        updateValue< double >( d, names::w_0, w_0 );
        updateValue< double >( d, nest::names::std, std);
        updateValue< double >( d, names::G_h,G_h );
        updateValue< double >( d, names::G_a,G_a );
        updateValue< double >( d, nest::names::V_reset, V_reset );
        updateValue< double >( d, nest::names::t_ref, t_ref );
        if ( t_ref < 0 ){
            throw nest::BadProperty("The refractory time must be at least one simulation step." );
        }
    }

    void vestibular_regular::State_::get( DictionaryDatum& d ) const {
        ( *d )[ nest::names::V_m ] = V_m;
        ( *d )[ nest::names::V_th ] = V_th;
        ( *d )[ nest::names::I_syn ] = I_syn;
        ( *d )[ names::hv ] = HV;
    }

    void vestibular_regular::State_::set( const DictionaryDatum& d,const Parameters_& p ){
        updateValue< double >( d, nest::names::V_m, V_m );
        updateValue< double >( d, names::hv, HV );
    }

    vestibular_regular::Buffers_::Buffers_( vestibular_regular& n )
        :logger_( n ){}
    vestibular_regular::Buffers_::Buffers_( const Buffers_&, vestibular_regular& n )
        :logger_( n ){}
    /* ----------------------------------------------------------------
     * Default and copy constructor for node
     * ---------------------------------------------------------------- */
    vestibular_regular::vestibular_regular()
        :Archiving_Node(), P_(), S_( P_ ), B_( *this ){
        recordablesMap_.create();
    }

    vestibular_regular::vestibular_regular( const vestibular_regular& n )
        :Archiving_Node( n ), P_( n.P_ ), S_( n.S_ ), B_( n.B_, *this ){
        recordablesMap_.create(); // useless?? 
    }

    /* ----------------------------------------------------------------
     * Node initialization functions
     * ---------------------------------------------------------------- */
    void vestibular_regular::init_state_( const Node& proto ) {
        const vestibular_regular& pr = downcast< vestibular_regular >( proto );
        S_ = pr.S_;
    }

    void vestibular_regular::init_buffers_(){
        B_.spikes.clear();   // includes resize
        B_.currents.clear(); // include resize
        B_.logger_.reset();  // includes resize
    }

    void vestibular_regular::calibrate(){
        B_.logger_.init();

        S_.HV = P_.HV;
        V_.dt = nest::Time::get_resolution().get_ms();

        // refractory time in steps
        V_.t_ref_steps = nest::Time( nest::Time::ms( P_.t_ref ) ).get_steps();
        assert( V_.t_ref_steps >= 0 ); // since t_ref_ >= 0, this can only fail in error
    }
    /* ----------------------------------------------------------------
     * Update and spike handling functions
     * ---------------------------------------------------------------- */
    void vestibular_regular::update( nest::Time const& slice_origin,const long from_step,const long to_step ){
        for ( long lag = from_step; lag < to_step; ++lag ) {
            S_.I_syn = P_.I_bias + P_.std * V_.randomDevice(nest::kernel().rng_manager.get_rng());
            S_.Xa += ( - S_.Xa + S_.HV ) / P_.tau_a * V_.dt ;
            // update membrane potential
            if ( S_.refr_count == 0 ){ // neuron absolute not refractory
                S_.I_syn += P_.G_h * S_.HV - P_.G_a * S_.Xa;  
                S_.V_m += ( - S_.V_m + S_.I_syn ) / P_.tau_v * V_.dt ; 
            }else{ // count down refractory time
                --S_.refr_count;
            } 
            S_.V_th += ( P_.w_0 - S_.V_th ) / P_.tau_w * V_.dt ;
            // check for threshold crossing
            if ( S_.V_m >= S_.V_th ){
                // reset neuron
                S_.refr_count = V_.t_ref_steps;
                S_.V_m = P_.V_reset;
                S_.V_th += P_.delta_w;

                // send spike, and set spike time in archive.
                set_spiketime( nest::Time::step( slice_origin.get_steps() + lag + 1 ) );
                nest::SpikeEvent se;
                nest::kernel().event_delivery_manager.send( *this, se, lag );
            }
            // log membrane potential
            B_.logger_.record_data( slice_origin.get_steps() + lag );
        }
    }

    void vestibular_regular::handle( nest::SpikeEvent& e ){
        assert( false && (bool)(void*)"Not accepting input spikes" );
    }
    void vestibular_regular::handle( nest::CurrentEvent& e ){
        assert( false && (bool)(void*)"Not accepting input current" );
    }
    void vestibular_regular::handle( nest::DataLoggingRequest& e ){
        B_.logger_.handle( e );
    }
}

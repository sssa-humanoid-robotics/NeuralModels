/*
 *  sensorsmodule.cpp
 *
 *  Copyright (C) 2018 Lorenzo Vannucci
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sensors_module.h"

// Generated includes:
#include "config.h"

#include "muscle_spindle.h"
#include "vestibular_regular.h"
#include "vestibular_irregular.h"

// Includes from nestkernel:
#include "dynamicloader.h"
#include "exceptions.h"
#include "genericmodel.h"
#include "genericmodel_impl.h"
#include "kernel_manager.h"
#include "model.h"
#include "model_manager_impl.h"
#include "nestmodule.h"
#include "target_identifier.h"

// Includes from sli:
#include "booldatum.h"
#include "integerdatum.h"
#include "sliexceptions.h"
#include "tokenarray.h"

// -- Interface to dynamic module loader ---------------------------------------
#if defined( LTX_MODULE ) | defined( LINKED_MODULE )
sensors::SensorsModule sensors_module_LTX_mod;
#endif

// -- DynModule functions ------------------------------------------------------

sensors::SensorsModule::SensorsModule() {
#ifdef LINKED_MODULE
    nest::DynamicLoaderModule::registerLinkedModule( this );
#endif
}

sensors::SensorsModule::~SensorsModule() {}

const std::string sensors::SensorsModule::name( void ) const {
    return std::string( "Sensors Module" ); // Return name of the module
}

const std::string sensors::SensorsModule::commandstring( void ) const {
    // Instruct the interpreter to load sensors_module-init.sli
    return std::string( "(sensors_module-init) run" );
}

//-------------------------------------------------------------------------------------

void sensors::SensorsModule::init(SLIInterpreter*) {
    nest::kernel().model_manager.register_node_model< muscle_spindle >("muscle_spindle");
    nest::kernel().model_manager.register_node_model< vestibular_regular >("vestibular_regular");
    nest::kernel().model_manager.register_node_model< vestibular_irregular >("vestibular_irregular");
}

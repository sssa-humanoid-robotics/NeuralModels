/*
 *  vestibular_irregular.cpp
 *
 *  Copyright (C) 2018 Mattia Villani
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "vestibular_irregular.h"

#include "sensors_names.h"

namespace sensors {

vestibular_irregular::vestibular_irregular(){ 

    DictionaryDatum dic = DictionaryDatum( new Dictionary );
    def<double>(dic, names::I_bias, 0.049 );
    def<double>(dic, names::delta_w, 0.001 );
    def<double>(dic, nest::names::std, 0.0015 );
    def<double>(dic, names::G_h, 0.0315 );
    def<double>(dic, names::G_a, 0.0315 );
    set_status(dic);

}

}